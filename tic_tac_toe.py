import numpy as np

def winner_for_combination(a, b, c):
    if a == 0 or b == 0 or c == 0:
        return 0
    elif a == b == c:
        return a
    else:
        return 0 

def winner(board):
    ''' COLUMNS '''  

    for i in range(0,3):
        vertical_winner = winner_for_combination(*board[:,i])
        if vertical_winner:
            return vertical_winner

    ''' ROWS '''  

    for i in range(0,3):
        horizontal_winner = winner_for_combination(*board[i,:])
        if horizontal_winner:
            return horizontal_winner

    ''' DIAGONALS '''

    diagonal1 = winner_for_combination(board[0,0], board[1,1], board[2,2])
    diagonal2 = winner_for_combination(board[2,0], board[1,1], board[0,2])
    return diagonal1 or diagonal2
        
''' 
if 1 then x is winner
if 2 then o is winner
if 0 then no winner
'''

def printBoard(board):
    q = ''
    for j in range(3):
        if board[0,j] == 0:
            q += '- '
        elif board[0,j] == 1:
            q += 'x '
        elif board[0,j] == 2:
            q += 'o '
    q += '\n'        
    for j in range(3):
        if board[1,j] == 0:
            q += '- '
        elif board[1,j] == 1:
            q += 'x '
        elif board[1,j] == 2:
            q += 'o '
    q += '\n'         
    for j in range(3):
        if board[2,j] == 0:
            q += '- '
        elif board[2,j] == 1:
            q += 'x '
        elif board[2,j] == 2:
            q += 'o '                        
    print(q)        

def get_user_input(current_player_sign,board):
    while True:
        try:
            a = int(input(f'Enter first coordinate for player {current_player_sign}: '))-1
            b = int(input(f'Enter second coordinate for player {current_player_sign}: '))-1
        except ValueError:
            print("Invalid input, try again")
            continue   

        if a>=0 and a<=2 and b>=0 and b<=2 and board[a,b] == 0:
            return a,b
        else:
            print('Invalid input, try again')    

def game():
    board = np.zeros((3, 3))
    i = 0
    while winner(board) == 0 and i < 9:
        current_player_sign = 'x' if i%2 == 0 else 'o'
        a, b = get_user_input(current_player_sign,board)
        board[a,b] = 1 if i%2 == 0 else 2
        printBoard(board)
        i += 1
    w = winner(board)    
    if w == 0:
        print('draw')
    elif w == 1:
        print('x won')
    elif w == 2:
        print('o won')      

game()          
